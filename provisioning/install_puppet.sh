#!/bin/bash

# only install puppet package if not installed
if ! dpkg -s puppet-common; then
    wget http://apt.puppetlabs.com/puppetlabs-release-precise.deb >> /dev/null
    sudo dpkg -i puppetlabs-release-precise.deb >> /dev/null
    sudo apt-get update >> /dev/null
    sudo apt-get install puppet-common -y -q >> /dev/null
    rm -f puppetlabs-release-precise.deb
fi

